import React from 'react';
import { QueryRenderer, graphql } from 'react-relay';
import environment from './environment'
import { render } from 'react-dom';
import './index.css';
import App from './App';

render(
    <QueryRenderer
        environment={environment}
        query={graphql`
          query srcQuery {
            viewer {
              ...App_viewer
            }
          }
        `}
        variables={{}}
        render={({ error, props }) => {
            if (props) {
                return <App viewer={props.viewer}/>;
            } else {
                return <div>Loading</div>;
            }
        }}
    />,
    document.getElementById('root')
);
