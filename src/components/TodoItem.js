// @flow
import * as React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import classNames from 'classnames'
import RemoveTodoMutation from '../mutations/RemoveTodoMutation';
import EditTodoMutation from '../mutations/EditTodoMutation';

const ENTER_KEY = 13;
const ESCAPE_KEY = 27;

type Props = {
    todo: Object,
    onTodoChange(number, string): void,
    onTodoToggle(number): void,
    onTodoDelete(number): void,
}

type State = {
    isEditing: boolean,
    editedText: string
}

class TodoItem extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isEditing: false,
            editedText: this.props.todo.text
        };
    }

    input : ?HTMLInputElement

    handleEdit = () => {
        const input = this.input

        if (!input) {
            return
        }

        this.setState(
            { isEditing: true },
            () => {
                input.focus();
                const tmp = input.value;
                input.value = '';
                input.value = tmp;
            }
        );
    }

    handleInputChange = (event: SyntheticEvent<HTMLInputElement>) => {
        const target : HTMLInputElement = event.currentTarget;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
    }

    handleSave = () => {
        const text = this.state.editedText
        if (text.length) {
            EditTodoMutation.commit(
                this.props.relay.environment,
                text,
                this.props.todo.id
            )
            this.setState({isEditing: false})
        }
    }

    handleKeydown = (event: SyntheticKeyboardEvent<HTMLInputElement>) => {
        const key = event.keyCode;

        if (key === ESCAPE_KEY) {
            this.setState({
                editedText: this.props.text,
                isEditing: false
            });
        } else if (key === ENTER_KEY) {
            this.handleSave()
        }
    }

    _removeTodo = () => {
        RemoveTodoMutation.commit(
            this.props.relay.environment,
            this.props.todo,
            this.props.viewer,
        );
    }
    
    render () {
        const {
            todo: {
                complete,
                text,
                id,
            },
            onTodoToggle,
        } = this.props;

        const mainClass = classNames({
            complete,
            editing: this.state.isEditing
        });

        return (
            <li className={mainClass}>
                <div className="view">
                    <input className="toggle" type="checkbox" checked={complete} onChange={() => onTodoToggle(id)} />
                    <label onDoubleClick={this.handleEdit}>{text}</label>
                    <button className="destroy" onClick={this._removeTodo} />
                </div>
                <input
                    ref={input => { this.input = input } }
                    autoFocus={true}
                    className="edit"
                    name="editedText"
                    value={this.state.editedText}
                    onChange={this.handleInputChange}
                    onKeyDown={this.handleKeydown}
                    onBlur={this.handleSave} />
            </li>
        )
    }
}

export default createFragmentContainer(
    TodoItem,
    graphql`
        fragment TodoItem_todo on Todo {
            id,
            text,
            complete
        },
        fragment TodoItem_viewer on User {
            id
        }
    `
)