// @flow

import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import TodoItem from './TodoItem'
import type { Todos } from '../types/todos'

type Props = {
    todos: Todos,
    onTodoChange() : void,
    onTodoDelete() : void,
    onTodoToggle() : void,
    onToggleAllChange() : void
}

const Main = ({
    viewer,
    onTodoChange,
    onTodoDelete,
    onTodoToggle,
    onToggleAllChange
} : Props) => {
    if(!viewer) {
        return <div>Loading...</div>
    }

    const { totalCount, completedCount, todos } = viewer
    // const todos = viewer && viewer.todos.edges.node



    return (
        <section className="main">
            <input id="toggle-all"
                   checked={completedCount === totalCount}
                   className="toggle-all"
                   type="checkbox"
                   onChange={onToggleAllChange} />
            <label htmlFor="toggle-all">Mark all as complete</label>
            <ul className="todo-list">
                {/* These are here just to show the structure of the list items */}
                {/* List items should get the class `editing` when editing and `completed` when marked as completed */}
                {todos && todos.edges.map(({node}) => (
                  <TodoItem
                    key={node.id}
                    todo={node}
                    viewer={viewer}
                    onTodoChange={onTodoChange}
                    onTodoToggle={onTodoToggle}
                    onTodoDelete={onTodoDelete}
                  />
                ))}
            </ul>
        </section>
    )
};

export default createFragmentContainer(
    Main,
    graphql`
        fragment Main_viewer on User {
            todos (
                first: 2147483647
            ) @connection(key: "Main_todos") {
                edges {
                    node {
                        ...TodoItem_todo
                    }
                }
            },
            totalCount,
            completedCount,
            ...TodoItem_viewer
        }
    `
)

