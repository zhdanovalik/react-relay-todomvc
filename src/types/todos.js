// @flow

export type Id = number;
export type Value = string;

export type Todo = {
    +id: Id,
    +value: Value,
    +completed: boolean
}

export type Todos = Array<Todo>

export type TodosState = {
    +todos: Todos
};

export const ADD_TODO : 'ADD_TODO' = 'ADD_TODO';
export const REMOVE_TODO : 'REMOVE_TODO'  = 'REMOVE_TODO';
export const CHANGE_TODO : 'CHANGE_TODO'  = 'CHANGE_TODO';
export const TOGGLE_TODO : 'TOGGLE_TODO'  = 'TOGGLE_TODO';
export const TOGGLE_ALL_TODOS : 'TOGGLE_ALL_TODOS'  = 'TOGGLE_ALL_TODOS';
export const REMOVE_COMPLETED_TODOS : 'REMOVE_COMPLETED_TODOS' = 'REMOVE_COMPLETED_TODOS';
export const SET_STATE : 'SET_STATE' = 'SET_STATE';
  
export type AddTodoAction = { type: typeof ADD_TODO, value: string };
export type RemoveTodoAction = { type: typeof REMOVE_TODO, id: number };
export type EditTodoAction = { type: typeof CHANGE_TODO, id: number, value: string };
export type ToggleTodoAction = { type: typeof TOGGLE_TODO, id: number };
export type ToggleAllTodoAction = { type: typeof TOGGLE_ALL_TODOS, completed: boolean };
export type RemoveCompletedTodos = { type: typeof REMOVE_COMPLETED_TODOS};
export type SetState = { type: typeof SET_STATE, todos: Todos}

export type TodoAction =
    | AddTodoAction
    | RemoveTodoAction
    | EditTodoAction
    | ToggleTodoAction
    | ToggleAllTodoAction
    | RemoveCompletedTodos
    | SetState