import {
    commitMutation,
    graphql,
} from 'react-relay';
import {ConnectionHandler} from 'relay-runtime';

const mutation = graphql`
    mutation EditTodoMutation($input: RenameTodoInput!) {
        renameTodo(input: $input) {
            todo {
                id,
                text
            }
        }
    }
`;

function commit(
    environment,
    text,
    id
) {
    return commitMutation(
        environment,
        {
            mutation,
            variables: {
                input: {id, text},
            },
            optimisticUpdater: () => ({
                renameTodo: {
                    todo: {
                        id,
                        text
                    }
                }
            }),
        }
    );
}

export default {commit};