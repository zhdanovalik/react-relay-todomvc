// @flow

import {
    VISIBILITY_ALL
} from '../types/visibility'

import type { ChangeFilter, Filter } from '../types/visibility'

export const CHANGE_VISIBILITY_FILTER = 'CHANGE_VISIBILITY_FILTER';

const visibility = (state : Filter = VISIBILITY_ALL, action: ChangeFilter) => {
    switch (action.type) {
        case CHANGE_VISIBILITY_FILTER:
            return action.filter;
        default:
            return state;
    }
};

export default visibility