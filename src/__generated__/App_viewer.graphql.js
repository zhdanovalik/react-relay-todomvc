/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
type Main_viewer$ref = any;
import type { FragmentReference } from 'relay-runtime';
declare export opaque type App_viewer$ref: FragmentReference;
export type App_viewer = {|
  +id: string,
  +totalCount: ?number,
  +completedCount: ?number,
  +$fragmentRefs: Main_viewer$ref,
  +$refType: App_viewer$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "App_viewer",
  "type": "User",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "id",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "totalCount",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "ScalarField",
      "alias": null,
      "name": "completedCount",
      "args": null,
      "storageKey": null
    },
    {
      "kind": "FragmentSpread",
      "name": "Main_viewer",
      "args": null
    }
  ]
};
(node/*: any*/).hash = '11f707085e41708ac9dcc463cc6e099e';
module.exports = node;
