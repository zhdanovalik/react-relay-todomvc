// @flow

import * as React from 'react';
import { graphql, createFragmentContainer } from 'react-relay'
import 'todomvc-app-css/index.css'
import {
    VISIBILITY_ACTIVE,
    VISIBILITY_COMPLETED
} from './actions/visibility'

// components
import Main from './components/Main'
import Footer from './components/Footer'
import type {
    TodoAction,
    AddTodoAction,
    RemoveTodoAction,
    EditTodoAction,
    ToggleTodoAction,
    ToggleAllTodoAction,
    RemoveCompletedTodos,
    SetState
} from './actions/todos'
import type { filter, ChangeFilter } from './actions/visibility'
import type { Todo, Todos } from './types/todos'

const ENTER_KEY = 13


type Props = {
    todos: Todos,
    visibility: filter,
    allCompleted: boolean,
    actions: {
        addTodo(string): AddTodoAction,
        removeTodo(number) : RemoveTodoAction,
        editTodo(number, string) : EditTodoAction,
        toggleTodo(boolean): ToggleTodoAction,
        toggleAllTodos(boolean): ToggleAllTodoAction,
        removeCompletedTodos(): RemoveCompletedTodos,
        setState(Todos): SetState,
        changeFilter(string): ChangeFilter
    }
}

type State = {
    todoInput: string,
    toggleAll: boolean
}

class App extends React.Component<Props, State> {
    constructor(props) {
        super(props);
        this.state = {
            todoInput: '',
            toggleAll: false
        }
    }

    handleChange = (event: SyntheticInputEvent<HTMLInputElement>) : void => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    onToggleAllChange = () => {
        // this.props.actions.toggleAllTodos(!this.props.allCompleted)
    }

    handleSubmit = (event: SyntheticKeyboardEvent<HTMLInputElement>) => {
        if (event.keyCode !== ENTER_KEY) {
            return
        }

        event.preventDefault()

        // this.props.actions.addTodo(
        //     this.state.todoInput.trim()
        // )

        this.setState({todoInput: ''})
    }

  render() {
    const { todos, visibility, actions, allCompleted } = this.props;

    // const filterFunc = (todos, filter) => {
    //     if(filter === VISIBILITY_COMPLETED) {
    //         return todos.filter(todo => todo.completed)
    //     } else if (filter === VISIBILITY_ACTIVE) {
    //         return todos.filter(todo => !todo.completed)
    //     } else {
    //         return todos
    //     }
    // };
    //
    // const filteredTodos = filterFunc(todos, visibility)

      const hasTodods = this.props.viewer.totalCount

      const editTodo = () => {}
      const toggleTodo = () => {}
      const removeTodo = () => {}

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
            className="new-todo" 
            placeholder="What needs to be done?"
            name="todoInput"
            value={this.state.todoInput}
            onChange={this.handleChange}
            onKeyDown={this.handleSubmit}
            autoFocus />
        </header>
        {/* This section should be hidden by default and shown when there are todos */}
        {hasTodods > 0 &&
          <Main
            viewer={this.props.viewer}
            onToggleAllChange={this.onToggleAllChange}
            onTodoChange={editTodo}
            onTodoToggle={toggleTodo}
            onTodoDelete={removeTodo}/>
        }

        {/* This footer should hidden by default and shown when there are todos */}
        {/*{todos.length > 0 &&*/}
          {/*<Footer todos={todos}*/}
                  {/*visibility={visibility}*/}
                  {/*changeFilter={changeFilter}*/}
                  {/*clear={removeCompletedTodos}/>*/}
        {/*}*/}
      </section>  
    );
  }
}



export default createFragmentContainer(
    App,
    graphql`
        fragment App_viewer on User {
            id,
            totalCount,
            completedCount,
            ...Main_viewer
        }
    `
)
