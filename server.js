process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

import express from 'express';
import graphQLHTTP from 'express-graphql';
import {schema} from './data/schema';

const GRAPHQL_PORT = 9998;

// Expose a GraphQL endpoint
const graphQLServer = express();
graphQLServer.use('/', graphQLHTTP({schema, pretty: true}));
graphQLServer.listen(GRAPHQL_PORT, () => console.log(
    `GraphQL Server is now running on http://localhost:${GRAPHQL_PORT}`
));
